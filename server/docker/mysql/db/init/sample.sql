CREATE DATABASE IF NOT EXISTS test_database;
USE test_database;

CREATE USER 'testuser'@'localhost' IDENTIFIED BY 'testuser';
GRANT ALL PRIVILEGES ON * . * TO 'testuser'@'localhost';