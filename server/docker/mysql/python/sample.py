# from sqlalchemy import create_engine
import mysql.connector

# connect mysql
cnx = mysql.connector.connect(
    host='192.168.2.2',
    port='3306',
    user='testuser',
    password='testuser',
    database='test_database'
)

cursor = cnx.cursor()

# create new database
query = "create database if not exists test_database"
cursor.execute(query)
# create new table
query2 = "create table if not exists test (id int, data varchar(20))"
cursor.execute(query2)
# check table
query3 = "SHOW TABLES"
cursor.execute(query3)
print(cursor.fetchall())
