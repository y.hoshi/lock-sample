#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import threading

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import BigInteger
from sqlalchemy.sql.sqltypes import Text
from sqlalchemy.engine import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.exc import SQLAlchemyError

'''
悲観ロック
'''
Base = declarative_base()


class Account(Base):
    __tablename__ = 'accounts'
    # ID
    id = Column(BigInteger, primary_key=True)
    # 口座の持ち主
    name = Column(Text, nullable=False)
    # 残高
    cash = Column(BigInteger, nullable=False)


def _update(session, name, cash_delta, wait):
    '''
    口座の残高を更新する関数
    :param Session session: セッション
    :param str name: 口座の持ち主
    :param int cash_delta: 更新する金額
    :param int wait: 更新するまでのディレイ
    '''
    try:
        with session.begin(subtransactions=True):
            # SELECT 時に排他ロックを取得する
            query = session.query(Account).with_for_update()
            # 口座の持ち主から絞り込む
            account = query.filter_by(name=name).one()
            # スリープをかける
            time.sleep(wait)
            # 口座の残高を更新する
            account.cash += cash_delta
    except SQLAlchemyError:
        session.rollback()


def main():
    # 発行SQL確認のためecho=True
    engine = create_engine(
        'mysql://root:root@192.168.2.2:3306/test_database',
        echo=True
    )

    # テーブルを作成する
    Base.metadata.create_all(engine)

    SessionMaker = sessionmaker(
        bind=engine,
        autocommit=True,
        expire_on_commit=False,
    )

    session = SessionMaker()

    # 動作確認用にレコードを1つ追加
    with session.begin(subtransactions=True):
        account = Account(name='test', cash=100)
        session.add(account)

    # ロック確認のためセッション（ユーザ）を2つ作成
    t1 = threading.Thread(target=_update, args=(SessionMaker(), 'test', 50, 1))
    t2 = threading.Thread(target=_update, args=(SessionMaker(), 'test', 100, 0))

    # ユーザ1人目が更新開始
    t1.start()
    # ユーザ2人目が更新開始
    t2.start()


if __name__ == '__main__':
    main()