# SQLAlchemy の排他制御検証

### コンテナの立ち上げ方

```
cd server/docker/mysql
docker-compose build
docker-compose up -d
```

### 稼働確認

Up になってれば OK

```
docker-compose ps
```

以下実行結果

```
 Name               Command             State               Ports
------------------------------------------------------------------------------
mysql     docker-entrypoint.sh mysqld   Up      0.0.0.0:3306->3306/tcp,
                                                33060/tcp
python3   python3                       Up

```

### MySQL の IP 確認方法

```
docker exec -it mysql sh
hostname -i
```

### MySQL のコマンドラインまで入る

```
docker exec -it mysql sh
mysql -u root -p
Enter password: rootを入力
```

### Python の実行まで

```
docker exec -it python3 sh
python3 [実行ファイル名]
```

- lock1.py
  悲観ロックの検証スクリプト

- lock2.py
  楽観ロックの検証スクリプト

### 流れ

##### 悲観ロック(lock1.py)

- レコード(cash = 100)を 1 つ追加
- t1 がレコードをロックしに行く
- t2 がレコードを更新しに行くが、t1 がロックしているため止まる
- t1 が cash(100) に 50 を加算する更新処理が実行される(cash = 150)
- t1 の更新作業が終わったので t2 の cash(150) に 100 を加算する更新処理が実行される(cash = 250)

##### 楽観ロック(lock2.py)

- レコード(cash = 100)を 1 つ追加
- t1 がレコードを更新しに行く
- t1 が sleep で 1 秒待っている間に t2 が先にレコードを更新(cash = 100 + 100)する
- t1 が更新処理を実行しようとした際に t2 のトランザクションがレコードを変更したことを検知してロールバックをする(加算処理は実行されないため cash は 200 のまま)

sqlalchemy の echo を True に設定しているので SQL の実行ログが吐き出されると思います
実行フローはそちらで確認して実際の処理結果に関しては db を参照してください
現状　ユーザ名=test で条件かけちゃってるので再度実行する際にはテーブルのレコードを削除するか、スクリプトを適当にいじってください

[これ](https://qiita.com/t_okkan/items/ce9d145750cd07e70606#%E6%A5%BD%E8%A6%B3%E7%9A%84%E6%8E%92%E4%BB%96%E5%88%B6%E5%BE%A1)も参考になるかも
